#!/bin/bash
#
#

# Add error codes only from driver.bash to output variable
output=$(bash driver.bash 2>&1 | grep "error" | awk '{print substr($0, 1, 3)}')

# Set API endpoint variable
endpoint="https://yourapihere.com"

# Set output file variable
output_file="$(dirname "${BASH_SOURCE[0]}")/output.txt"

# Add newline separation to each error code
formatted_output=$(echo "$output" | sed 's/$/\n/' | awk NF)

# Loop through each error code
while IFS= read -r error_code; do
    url="$endpoint?code=$error_code"
    response=$(curl -s -X POST "$url")
    echo "Response for $error_code: $response"
    echo "Response for $error_code: $response" >> "$output_file"
done <<< "$formatted_output"


