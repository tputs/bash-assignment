#!/bin/bash
# vim:tw=0:
#
# This is the driver program for the assignment.
#
# DESCRIPTION OF THIS DRIVER
# This script generates some output which you will then use in the script that
# you write. Key points of this script:
# 1. It simulates a very simplistic program that is writing log output.
# 2. Two kinds of log messages are written:
# 2a. A three-digit integer between 1 and 10 (padded with zeros) followed by
# a space and the word ok and a newline.  These are written to STDOUT.
# 2b. A three-digit integer between 101 and 110 followed by a space and the
# word error and a newline. These are written to STDERR.
# 3. This script itself has at least 3 different intentional coding errors in
# it.  It will run as-is but it does not run as-described.  Identify them and
# fix them.  Write comments in this script itself to desribe what was broken
# and how you fixed it.  The comments should be placed at or around where the
# change was made.
#
# Added bonus: commit this script to some git repo as-is, make your changes,
# commit your changes and then show the git diff between your changes and the
# original.  THIS PART IS NOT REQUIRED, but try it if you have spare time.

for ((i=1;i<10;++i)) ; do
	printf '%03d ok\n' $((i)) 		# \n added so the script creates a new line after each statement, $(( )) used over depricated $[]
	printf '%03d error\n' $((i+100)) >&2 	# >&2 moved to error print line, was incorrectly printing OK to STDERR
done

# USAGE OF THIS DRIVER SCRIPT
# Once you have repaired this script so that it works as intended, you will
# use this script.  This script generates input which you will consume and
# process in another shell script that you will write yourself.
#
# Make a directory on your system for this exercise. Put this script there.
# Also create your script there -- so this driver and your script are in the
# same directory.
#
# Requiremetns for the shell script you will write are as follows:
#
# 1. It must be written in bash.
# 2. It must be written on a UNIX/Linux or UNIX/Linux-ish system (macOS would
# be fine).
# 3. Do not use temporary files of any kind.
# 3. Your script must execute this driver script and process the driver output
# as follows:
# 3a. Iterate over the "error" output ONLY.
# 3b. For each error, capture the error CODE (number) only.
# 3c: Issue a curl to the endpoint https://yourapihere.com?code=THE_CODE in
# such a way that you ONLY get response data from the endpoint; we do not want
# any diagnostics or progress output from curl itself.  Just. The. Response.
# Of course, here THE_CODE is the error code from the driver output line you
# are currently processing.
# 3d. Append the curl output to a file in the same directory as the your
# script and the driver script.
#
# Once you have all of this in working order and have the curl output
# accumulated, zip (or tar, or whatever) all files from the exercise and send
# them along to me.
#
# HINTS:
# Pipelines are your friend.
# Be wary of quotes and spaces.
# Spellign errors in the comments are not among the intentional driver script
# errors spoke about above. ;)


